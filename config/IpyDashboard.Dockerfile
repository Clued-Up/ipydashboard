
FROM python:3.7-alpine3.7

RUN apk update
RUN apk upgrade
RUN apk add build-base
RUN apk add py-pip
RUN pip install --upgrade pip
RUN pip install requests

ENV MAIN_PATH=/usr/local/bin/IpyDashboard
ENV LIBS_PATH=${MAIN_PATH}/libs
ENV CONFIG_PATH=${MAIN_PATH}/config
ENV NOTEBOOK_PATH=${MAIN_PATH}/notebooks

RUN pip install jupyterlab

RUN apk add --update nodejs
RUN npm config set user 0 && npm config set unsafe-perm true && npm install -g ijavascript && ijsinstall
    
# Install the magic wrapper.
ADD config/wrapdocker /usr/local/bin/wrapdocker

# Install Docker and dependencies
RUN apk --update add   bash   iptables   ca-certificates   e2fsprogs   docker   && chmod +x /usr/local/bin/wrapdocker   && rm -rf /var/cache/apk/*

# Define additional metadata for our image.
VOLUME /var/lib/docker
    
RUN pip install bash_kernel
RUN python -m bash_kernel.install
    

RUN pip install nbsphinx --upgrade

ENV BUILD_DEPS     alpine-sdk     coreutils     ghc     gmp     libffi     linux-headers     musl-dev     wget     zlib-dev
ENV PERSISTENT_DEPS     graphviz     openjdk8     python     py2-pip     sed     ttf-droid     ttf-droid-nonlatin
ENV EDGE_DEPS cabal

ENV PLANTUML_VERSION 1.2017.18
ENV PLANTUML_DOWNLOAD_URL https://sourceforge.net/projects/plantuml/files/plantuml.$PLANTUML_VERSION.jar/download

ENV PANDOC_VERSION 1.19.2.4
ENV PANDOC_DOWNLOAD_URL https://hackage.haskell.org/package/pandoc-$PANDOC_VERSION/pandoc-$PANDOC_VERSION.tar.gz
ENV PANDOC_ROOT /usr/local/pandoc

ENV PATH $PATH:$PANDOC_ROOT/bin

# Create Pandoc build space
RUN mkdir -p /pandoc-build
WORKDIR /pandoc-build

# Install/Build Packages
RUN apk upgrade --update &&     apk add --no-cache --virtual .build-deps $BUILD_DEPS &&     apk add --no-cache --virtual .persistent-deps $PERSISTENT_DEPS &&     curl -fsSL "$PLANTUML_DOWNLOAD_URL" -o /usr/local/plantuml.jar &&     apk add --no-cache --virtual .edge-deps $EDGE_DEPS -X http://dl-cdn.alpinelinux.org/alpine/edge/community &&     curl -fsSL "$PANDOC_DOWNLOAD_URL" | tar -xzf - &&         ( cd pandoc-$PANDOC_VERSION && cabal update && cabal install --only-dependencies &&         cabal configure --prefix=$PANDOC_ROOT &&         cabal build &&         cabal copy &&         cd .. ) &&     rm -Rf pandoc-$PANDOC_VERSION/ &&     rm -Rf /root/.cabal/ /root/.ghc/ &&     rmdir /pandoc-build &&     set -x;     addgroup -g 82 -S www-data;     adduser -u 82 -D -S -G www-data www-data &&     mkdir -p /var/docs &&     apk del .build-deps .edge-deps

RUN pip install sphinx sphinx-autobuild
RUN pip install sphinx_rtd_theme

#add support to use md in sphinx
RUN pip install recommonmark
    
RUN pip install cookiecutter
RUN apk add git

EXPOSE 8887

CMD cd ${MAIN_PATH} && sh config/run_jupyter.sh
