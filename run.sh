#!/bin/bash
docker build -t ipydashboard -f config/IpyDashboard.Dockerfile .
docker run --privileged -ti -v ${PWD}:/usr/local/bin/IpyDashboard -p 8887:8888 ipydashboard