
import { ABCWidgetFactory, DocumentRegistry } from '@jupyterlab/docregistry';

import { DashboardWidget } from './widget'

import { DashboardPanel } from './panel'

/*
 * A widget factory for dashboards
 *
 * http://jupyterlab.github.io/jupyterlab/classes/_docregistry_src_default_.abcwidgetfactory.html
 * Implements IWidgetFactory<T: IDocumentWidget, U: IModel>
 *
 * Connects the DocumentWidget with the Model.
 */
export class DashboardWidgetFactory extends ABCWidgetFactory<DashboardPanel,DocumentRegistry.IModel> {
    /**
     * Construct a new widget factory.
     *
     * @param options - The options used to construct the factory.
     */
    constructor(options: DashboardWidgetFactory.IOptions) {
        super(options);
        
    }
      
    /**
     * Create a new widget.
     */
    protected createNewWidget(context: DocumentRegistry.IContext<DocumentRegistry.IModel>): DashboardPanel {
        
        let content = new DashboardWidget(context);
        let widget = new DashboardPanel({ context, content });

        return widget;
    }

}

/**
 * The namespace for `DashboardWidgetFactory` statics.
 */
export namespace DashboardWidgetFactory {
    /**
     * The options used to construct a `DashboardWidgetFactory`.
     */
    export interface IOptions extends DocumentRegistry.IWidgetFactoryOptions {
        
    }
}