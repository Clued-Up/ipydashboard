
import { getRoot } from './rootselector'
import { createSelector } from 'reselect'

// Actions
export function onLayoutChange(layout) {
    return {
        type: 'layout/set',
        payload: layout
    }
}

export function onAddTile() {
  return {
    type: 'tile/create',
    payload: undefined
  }
}

export function onRemoveOption(id) {
    return {
    type: 'tile/destroy',
    payload: id
  }
}



// the variable name must match the name of the reducer!!!
// returns the dict that contains the data of this reducer
export const getGridRoot = createSelector(
  getRoot,
  ({ gridReducer }) => gridReducer
)

export const getLayout = createSelector(
    getGridRoot,
    ({ layout }) => layout
)


// Reducer
const defaultState = {
    layout: [
          {i: 'a', x: 0, y: 0, w: 1, h: 2, static: true},
          {i: 'b', x: 1, y: 0, w: 3, h: 2, minW: 2, maxW: 4},
          {i: 'c', x: 4, y: 0, w: 1, h: 2}
        ],
	selected: null
}

export default function gridReducer(state = defaultState, { type, payload }) {
    switch (type) {
        case 'layout/set':
            return {...state, layout: payload}
        case 'layout/add':
            return [...state.layout, {i: 'd', x: 4, y: 0, w: 1, h: 2}];
        default:
          return state
      }
}
