
import { getRoot } from './rootselector'
import { createSelector } from 'reselect'

// Actions
export function onSelectStep(option) {
  return {
    type: 'step/set',
    payload: option
  }
}

// the variable name must match the name of the reducer!!!
// returns the dict that contains the data of this reducer
export const getCellsRoot = createSelector(
  getRoot,
  ({ cellsReducer }) => cellsReducer
)

export const getSelectedStep = createSelector(
    getCellsRoot,
    ({ step }) => step
)


// Reducer
const defaultState = {
    step: "none"
}

export default function cellsReducer(state = defaultState, { type, payload }) {
    switch (type) {
        case 'step/set':
          return {...state, step: payload}
        default:
          return state
      }
}