import * as React from 'react'
import * as Redux from 'redux';
import { connect } from 'react-redux'
import * as ReactGridLayout from 'react-grid-layout';

import { getLayout, onLayoutChange } from './ducks/grid'



interface StateProps {
    layout: Array<any>, // the actual list of tiles
    
}
     
interface DispatchProps { 
    onLayoutChange: (layout: Array<any>) => void
}

interface ArgProps {
    setOnAddTileListener: (onAddTileListener: () => void) => void
}

 
type Props = StateProps & DispatchProps & ArgProps;

const ReactGrid = ReactGridLayout.WidthProvider(ReactGridLayout);



const Grid: React.SFC<Props> = (props) => {
    const layoutDash = (layout, oldItem, newItem) => {
        const changed = !(oldItem.x == newItem.x && oldItem.y == newItem.y && oldItem.w == newItem.w && oldItem.h == newItem.h);

        if(changed) {
            props.onLayoutChange(layout);
        }
    }

    const onAddTile = () => {
        console.log("add tile");
    }
    
    props.setOnAddTileListener(onAddTile);
    
    return <ReactGrid
                   className="layout"
				   layout={ props.layout }
                   cols={ 12 }
				   rowHeight={ 600 / 4 }
				   autoSize={true}
				   minH = {1}
				   minW = {1}
				   isDraggable = {true}
                   isResizable = {true}
				   useCSSTransforms = {true}
				   margin = { [ 10, 10 ] }
				   onDragStop = { layoutDash }
				   onResizeStop = { layoutDash }
        >{
            // render the tiles here
            props.layout.map(function(item) {
                let id = item.i;
                return (
                    <div key={id} >
                        <span className="text">{id}</span>
                    </div>
                );
            })
        }</ReactGrid>
}




function mapStateToProps(state: any, props: Props): StateProps {
    return {
        layout: getLayout(state)
    }
}
 
function mapDispatchToProps(dispatch: Redux.Dispatch<any>, props: Props): DispatchProps {
    return {
        onLayoutChange: (layout) => dispatch(onLayoutChange(layout)),
    }
   
}
  
export default connect<StateProps, DispatchProps, ArgProps>
  (mapStateToProps, mapDispatchToProps)(Grid)