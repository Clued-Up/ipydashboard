import {
  NotebookModelFactory
} from '@jupyterlab/notebook';

import { Contents } from '@jupyterlab/services';


export class DashboardModelFactory extends NotebookModelFactory {
  
    constructor(options: NotebookModelFactory.IOptions) {
        super(options);
        
    }

    get name(): string {
        return 'dashboard';
      }

    get contentType(): Contents.ContentType {
        return 'file';
    }
    
    
  get fileFormat(): Contents.FileFormat {
    return 'text';
  }
}