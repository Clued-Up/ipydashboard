

import { DocumentRegistry } from '@jupyterlab/docregistry';

import { Message } from '@phosphor/messaging';

import { PanelLayout, Widget } from '@phosphor/widgets';


import {
  ICellModel,
  Cell,
  IMarkdownCellModel,
  CodeCell,
  MarkdownCell,
  ICodeCellModel,
  RawCell,
  IRawCellModel,
    CodeCellModel
} from '@jupyterlab/cells';

import { StaticNotebook, NotebookModel } from '@jupyterlab/notebook'

import { IRenderMimeRegistry } from '@jupyterlab/rendermime';

export class NotebookPanelLayout extends PanelLayout {
    
    protected onUpdateRequest(msg: Message): void {
      // This is a no-op.
    }
  }

export class DashboardWidget extends Widget {
    
    private widget: CodeCell;
    private context: DocumentRegistry.Context;
    
    constructor(options: DashboardWidget.IOptions) {
        super();
        
        //console.log(options.context);
        
        this.run = this.run.bind(this);
        
        this.context = options.context;
        this.layout = new NotebookPanelLayout();
        
        
        const editorConfig = StaticNotebook.defaultEditorConfig.code;
        let model: ICodeCellModel = NotebookModel.defaultContentFactory.createCodeCell({});
        
        let rendermime = options.rendermime;
        const contentFactory = new StaticNotebook.ContentFactory();
        
        let cell_options = { editorConfig, model, rendermime, contentFactory };
        
        this.widget = new CodeCell(cell_options);
        (this.layout as PanelLayout).insertWidget(0, this.widget);
        
        this.context.session.changeKernel({id: "python3", name: "python3"});
        
        //console.log(options.context.session);
        
        //console.log(widget);
        
    }

    run(): void {
        console.log("run cell");
        
        CodeCell.execute(
            this.widget, this.context.session
        ).then(reply => {
            console.log("cell executed");
            console.log(this.widget);
            this.update();
        });
    }
}

export namespace DashboardWidget {
    export interface IOptions {
        rendermime: IRenderMimeRegistry,
        context: DocumentRegistry.Context
    }
}

