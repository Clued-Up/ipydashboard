
import {
    ABCWidgetFactory, DocumentWidget, DocumentRegistry
} from '@jupyterlab/docregistry';

import {
    NotebookModel, Notebook, INotebookModel
} from '@jupyterlab/notebook';

import {
    DashboardWidget
} from './widget'

   
import { Kernel, KernelMessage, Session } from '@jupyterlab/services';

import { Token } from '@phosphor/coreutils';

import { Message } from '@phosphor/messaging';

import { ISignal, Signal } from '@phosphor/signaling';

import { IClientSession } from '@jupyterlab/apputils';
import { ToolbarButton } from '@jupyterlab/apputils';

/**
 * The class name added to notebook panels.
 */
const NOTEBOOK_PANEL_CLASS = 'jp-NotebookPanel';

const NOTEBOOK_PANEL_TOOLBAR_CLASS = 'jp-NotebookPanel-toolbar';

const NOTEBOOK_PANEL_NOTEBOOK_CLASS = 'jp-NotebookPanel-notebook';



//DocumentRegistry.IModel
/**
 * DocumentWidget<DashboardWidget, INotebookModel>
 */ 
export class DashboardPanel extends DocumentWidget<DashboardWidget, DocumentRegistry.IModel> {
    /**
     * The content used by the widget. This is the main-area-widget
     */
    readonly content: DashboardWidget;

    
    /**
     * Construct a new notebook panel.
     */
    constructor(options: DocumentWidget.IOptions<DashboardWidget, DocumentRegistry.IModel>) {
        super(options);

        console.log("options.content -> ");
        console.log(options.content);
        
        // Set up CSS classes
        this.addClass(NOTEBOOK_PANEL_CLASS);
        this.toolbar.addClass(NOTEBOOK_PANEL_TOOLBAR_CLASS);
        this.content.addClass(NOTEBOOK_PANEL_NOTEBOOK_CLASS);

        let button = new ToolbarButton({
          className: 'myButton',
          iconClassName: 'fa forward',
          onClick: this.content.run,
          tooltip: 'Run'
        });
        
        this.toolbar.insertItem(0, 'run', button);
        
        // Set up things related to the context
        //this.content.model = this.context.model;
        
        /*
        Kernel.getSpecs().then(kernelSpecs => {
          console.log('Default spec:', kernelSpecs.default);
          console.log('Available specs', Object.keys(kernelSpecs.kernelspecs));
          // use the default name
          let options: Kernel.IOptions = {
            name: kernelSpecs.default
          };
        });*/
        
    }
    
    
    /**
     * The client session used by the panel.
     */
    get session(): IClientSession {
        return this.context.session;
    }

  
  
    
  /**
   * The model for the widget.
   
  get model(): INotebookModel {
    return this.content ? this.content.model : null;
  }*/

  /**
   * Dispose of the resources used by the widget.
   */
  dispose(): void {
    this.content.dispose();
    super.dispose();
  }

 

  
}