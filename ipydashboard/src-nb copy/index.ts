const version = "0068";

import {
  JupyterLab, JupyterLabPlugin
} from '@jupyterlab/application';

import {
  ICommandPalette
} from '@jupyterlab/apputils';

import {
  IFileBrowserFactory
} from '@jupyterlab/filebrowser';

import {
  ILauncher
} from '@jupyterlab/launcher';

import {
    CommandRegistry
} from '@phosphor/commands'

import {
    DashboardWidgetFactory
} from './widgetfactory'

// the id of the the command that creates a new dashboard
const CREATENEW_CMD = "ipydashboard:create-new";

// The name of the factory that creates editor widgets.
const WIDGETFACTORY = 'NotebookWidgetFactory';

// The name of the extension that is displayed
const EXTENSIONNAME = "IpyDashboard";

// the file-type name
const DASHBOARDFILE = "dashboard";

// The file extension
const FILEEXTENSION = '.ipynb';

// Caption of the command of adding a dashboard
const COMMAND_CAPTION = 'Create a new IpyDashboard file'

// Function to create a new untitled dashboard file, given the current working directory.
const createNewDashboard = (commands: CommandRegistry, cwd: string) => {
    console.log("request new dashboard")
    return commands.execute('docmanager:new-untitled', {
        path: cwd, type: 'notebook', ext: FILEEXTENSION, //type can be one of 'file', 'notebook'
    }).then(panel => {
        console.log("create new dashboard");
        return commands.execute('docmanager:open', {
            path: panel.path, factory: WIDGETFACTORY
        });
    });
}

const plugin: JupyterLabPlugin<void> = {
    id: 'ipydashboard:plugin',
    requires: [IFileBrowserFactory, ICommandPalette], 
    optional: [ILauncher],
    autoStart: true,
    activate: (app: JupyterLab,
               browserFactory: IFileBrowserFactory,
               palette: ICommandPalette,
               launcher: ILauncher | null) => {
        
        console.log('JupyterLab extension ipydashboard is activated!');
        console.log("VERSION__"+version);
        
        if (launcher) {
            launcher.add({
                command: CREATENEW_CMD,
                rank: 0,
                category: 'Other'
            });
        }
        
        const { commands } = app;

        
        
        // Add a command for creating a new dashboard file.
        commands.addCommand(CREATENEW_CMD, {
            label: EXTENSIONNAME,
            caption: COMMAND_CAPTION,
            execute: () => {
                let cwd = browserFactory.defaultBrowser.model.path;
                return createNewDashboard(commands, cwd);
            }
        });
        
        
        /*
        // Now register the document widget
        const factory = new DashboardWidgetFactory({
            name: WIDGETFACTORY, 
            fileTypes: ['notebook'], //DASHBOARDFILE
            defaultFor: [FILEEXTENSION]
        });
        
        app.docRegistry.addWidgetFactory(factory);

        // register the filetype
        app.docRegistry.addFileType({
            name: DASHBOARDFILE,
            displayName: EXTENSIONNAME,
            mimeTypes: ['application/ipyds'],
            extensions: [FILEEXTENSION],
            iconClass: 'jp-MaterialIcon jp-ImageIcon',
            fileFormat: 'json'
        });*/
        
        return;
    }
}
    
export default plugin;