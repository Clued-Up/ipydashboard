
import { ABCWidgetFactory, DocumentRegistry } from '@jupyterlab/docregistry';

import { DashboardWidget } from './widget'

import { DashboardPanel } from './panel'

import { ToolbarItems } from '@jupyterlab/notebook';

import { INotebookModel } from '@jupyterlab/notebook';

import { NotebookPanel } from '@jupyterlab/notebook';

import { StaticNotebook } from '@jupyterlab/notebook';

import { IEditorMimeTypeService } from '@jupyterlab/codeeditor';

import {
  RenderMimeRegistry,
  standardRendererFactories as initialFactories
} from '@jupyterlab/rendermime';

import { editorServices } from '@jupyterlab/codemirror';



/*
 * A widget factory for dashboards
 *
 * http://jupyterlab.github.io/jupyterlab/classes/_docregistry_src_default_.abcwidgetfactory.html
 * Implements IWidgetFactory<T: IDocumentWidget, U: IModel>
 *
 * Connects the DocumentWidget with the Model.
 */
export class DashboardWidgetFactory extends ABCWidgetFactory</*DashboardPanel,DocumentRegistry.IModel*/NotebookPanel,
  INotebookModel> {
    /**
     * Construct a new widget factory.
     *
     * @param options - The options used to construct the factory.
     */
    constructor(options: DashboardWidgetFactory.IOptions) {
        super(options);
        
    }
      
    
      
    /**
     * Create a new widget.
     */
    protected createNewWidget(context: DocumentRegistry.IContext</*DocumentRegistry.IModel*/INotebookModel>): NotebookPanel/*DashboardPanel*/ {
        
        let rendermime = new RenderMimeRegistry({ initialFactories }).clone({ resolver: context.urlResolver });
        
        let nbOptions = {
            rendermime,
          contentFactory: NotebookPanel.defaultContentFactory,
            mimeTypeService: editorServices.mimeTypeService,
          editorConfig: StaticNotebook.defaultEditorConfig
        };
        
        let editorFactory = editorServices.factoryService.newInlineEditor;
          let contentFactory = new NotebookPanel.ContentFactory({ editorFactory });
        let content = contentFactory.createNotebook(nbOptions);

        let widget = new NotebookPanel({ context, content });
        ToolbarItems.populateDefaults(widget);
        return widget;
        /*
        
        let content = new DashboardWidget(context);
        let widget = new DashboardPanel({ context, content });

        return widget;*/
    }

}

/**
 * The namespace for `DashboardWidgetFactory` statics.
 */
export namespace DashboardWidgetFactory {
    /**
     * The options used to construct a `DashboardWidgetFactory`.
     */
    export interface IOptions extends DocumentRegistry.IWidgetFactoryOptions {
        
    }
}