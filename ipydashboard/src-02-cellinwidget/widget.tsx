

import { DocumentRegistry } from '@jupyterlab/docregistry';

import { Message } from '@phosphor/messaging';

import { PanelLayout, Widget } from '@phosphor/widgets';


import {
  ICellModel,
  Cell,
  IMarkdownCellModel,
  CodeCell,
  MarkdownCell,
  ICodeCellModel,
  RawCell,
  IRawCellModel,
    CodeCellModel
} from '@jupyterlab/cells';

import { StaticNotebook, NotebookModel } from '@jupyterlab/notebook'

import { IRenderMimeRegistry } from '@jupyterlab/rendermime';

export class NotebookPanelLayout extends PanelLayout {
    
    protected onUpdateRequest(msg: Message): void {
      // This is a no-op.
    }
  }

export class DashboardWidget extends Widget {
    
    constructor(options: DashboardWidget.IOptions) {
        super();
        
        this.layout = new NotebookPanelLayout();
        
        let widget: Cell;
        
        const editorConfig = StaticNotebook.defaultEditorConfig.code;
        let model: ICodeCellModel = NotebookModel.defaultContentFactory.createCodeCell({});
        
        let rendermime = options.rendermime;
        const contentFactory = new StaticNotebook.ContentFactory();
        
        let cell_options = { editorConfig, model, rendermime, contentFactory };
        
        widget = new CodeCell(cell_options);
        (this.layout as PanelLayout).insertWidget(0, widget);
        
        
        
    }

}

export namespace DashboardWidget {
    export interface IOptions {
        rendermime: IRenderMimeRegistry,
        context: DocumentRegistry.Context
    }
}

