
import {
    ABCWidgetFactory, DocumentWidget, DocumentRegistry
} from '@jupyterlab/docregistry';

import {
    Widget
} from '@phosphor/widgets';

import {
    StaticNotebook
} from '@jupyterlab/notebook';

import {
  IChangedArgs, PathExt
} from '@jupyterlab/coreutils';

import {
  Message
} from '@phosphor/messaging';

import { 
    PromiseDelegate
} from '@phosphor/coreutils';

import * as React from 'react';
import * as ReactDOM from 'react-dom';


/**
 * The class name added to the extension.
 */
const CLASS_NAME = 'jp-IpyDashboard';
const DIRTY_CLASS = 'jp-mod-dirty';

export class DashboardWidget extends Widget {
    private div: any;
    private isReady: boolean;
    private name: string;
    readonly context: DocumentRegistry.Context;
    private _ready = new PromiseDelegate<void>();
    
    /**
     * The cell factory used by the widget.
     */
    readonly contentFactory: StaticNotebook.IContentFactory;
    
    constructor(context: DocumentRegistry.Context) {
        super();
        this.isReady = false;
        this.context = context;
      
        this.name = this.context.localPath.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
        this._onTitleChanged();
        context.pathChanged.connect(this._onTitleChanged, this);

        this.context.ready.then(() => { this._onContextReady(); });
        this.context.ready.then(() => { this._handleDirtyState(); });
      
        this.addClass(CLASS_NAME);
        this.div = document.createElement('div');
        this.div.id='root'+this.name;
        this.div.setAttribute('tabindex', '0');
        this.node.appendChild(this.div);
        
        this.contentFactory =StaticNotebook.defaultContentFactory;
    }

    /**
     * Handle `'activate-request'` messages.
     */
    protected onActivateRequest(msg: Message): void {
        this.div.focus();
    }
    
    /**
     * A message handler invoked on an `'after-attach'` message.
     */
    protected onAfterAttach(msg: Message): void {
        super.onAfterAttach(msg);
        if (this.isVisible) {
            this.update();
        }
    }
    
    protected onAfterShow(msg: Message): void {
        this._onContentChanged();
    }

    private _onContextReady() : void {
        this.isReady = true;
        const contextModel = this.context.model;
        //this.context.model.fromString('{"hosting": "pipeline"}');
        
        // Set the editor model value.
        this._onContentChanged();
        

        contextModel.contentChanged.connect(this._onContentChanged, this);
        contextModel.stateChanged.connect(this._onModelStateChanged, this);
        
        this._ready.resolve(void 0);
    }

    /**
     * Handle a change to the title.
     */
    private _onTitleChanged(): void {
        this.title.label = PathExt.basename(this.context.localPath);
    }

    private _onContentChanged() : void {
        
       if (this.isReady) {
            const json = this.context.model.toString();
            const data = json != null && json.length > 0 ? JSON.parse(json) : {};
            
            const logger = ({ getState }) => {
                return next => action => {
                    const returnValue = next(action)
            
                    const state: string = JSON.stringify(getState());
                    console.log('state after dispatch', state)
            
                    this.context.model.fromString(state);
                  
                    // This will likely be the action itself, unless
                    // a middleware further in chain changed it.
                    return returnValue
                  }
            }
           
            //const store = createStore(rootReducer, data, applyMiddleware(logger));

            new Promise<void>((resolve, reject) => {
                ReactDOM.render("Hello World",
                    document.getElementById('root'+this.name), () => {
                        resolve();
                });
            });

            //this._saveToContext();
        };
    }
    

    private _saveToContext() : void {
        //console.log("saved!");
        //this.context.model.fromString('{"hosting":"pipeline"}');
        //this.context.save()
    }

    private _onModelStateChanged(sender: DocumentRegistry.IModel, args: IChangedArgs<any>): void {
        if (args.name === 'dirty') {
            this._handleDirtyState();
        }
    }

    private _handleDirtyState() : void {
        if (this.context.model.dirty) {
            this.title.className += ` ${DIRTY_CLASS}`;
        } else {
            this.title.className = this.title.className.replace(DIRTY_CLASS, '');
        }
    }

    /**
     * A promise that resolves when the csv viewer is ready.
     */
    get ready(): Promise<void> {
        return this._ready.promise;
    }

    
    
}


