import {
  ILayoutRestorer, JupyterLab, JupyterLabPlugin
} from '@jupyterlab/application';

import {
  ICommandPalette, InstanceTracker, IInstanceTracker
} from '@jupyterlab/apputils';

import {
  IFileBrowserFactory
} from '@jupyterlab/filebrowser';

import {
  ILauncher
} from '@jupyterlab/launcher';

import {
  IMainMenu
} from '@jupyterlab/mainmenu';

import {
  Token
} from '@phosphor/coreutils';


import {
  NotebookPanel,
  NotebookWidgetFactory,
  NotebookModelFactory,
  NotebookActions,
    NotebookModel, INotebookModel
} from '@jupyterlab/notebook';

import {
  nbformat
} from '@jupyterlab/coreutils';


import { editorServices } from '@jupyterlab/codemirror';

import { Contents } from '@jupyterlab/services';

import {
  RenderMimeRegistry,
  standardRendererFactories as initialFactories
} from '@jupyterlab/rendermime';

import { ABCWidgetFactory, DocumentRegistry } from '@jupyterlab/docregistry';

import {
    DashboardWidgetFactory
} from './widgetfactory'

import '../style/index.css';

import {
  INotebookTracker,
  NotebookTracker,
    StaticNotebook, ToolbarItems
} from '@jupyterlab/notebook';

/**
 * Initialization data for the ipydashboard extension.

const extension: JupyterLabPlugin<void> = {
  id: 'ipydashboard',
  autoStart: true,
  activate: (app: JupyterLab) => {
    console.log('JupyterLab extension ipydashboard is activated!');
  }
}; */



/**
 * The name of the factory that creates editor widgets.
 */
const FACTORY = 'DashboardFactory';

/**
 * The file extension
 */
const FILEEXTENSION = '.ipyds';

/**
 * Caption of the command of adding a dashboard
 */
const COMMAND_CAPTION = 'Create a new IpyDashboard file'

//interface IpyDashboardTracker extends IInstanceTracker<DashboardWidget> {}
//export const IpyDashboardTracker = new Token<IpyDashboardTracker>('ipydashboard/tracker');

export class f extends NotebookModelFactory {
    constructor(options: NotebookModelFactory.IOptions) {
        super(options);
        console.log("construct");
    }

  
  get name(): string {
      console.log("getname");
    return 'dashboard';
  }

}

export class w extends NotebookWidgetFactory {
    constructor(options: NotebookWidgetFactory.IOptions) {
        super(options);
        console.log("construct widgetfactory");
        
        
    }
    
      protected createNewWidget(
        context: DocumentRegistry.IContext<INotebookModel>
      ): NotebookPanel {
            context.ready.then(() => { 
                console.log("context ready");
                
            });
            console.log("create new widget");
            
            return super.createNewWidget(context);
      }
    
    

    
}

/**
 * The editor tracker extension.
 */
const plugin: JupyterLabPlugin<void> = {//IpyDashboardTracker
    //activate,
    id: 'ipydashboard:plugin',
    requires: [IFileBrowserFactory, ICommandPalette, ILayoutRestorer], //IMainMenu, 
    optional: [ILauncher],
    //provides: IpyDashboardTracker,
    autoStart: true,
    activate: (app: JupyterLab,
            browserFactory: IFileBrowserFactory,
            palette: ICommandPalette,
            restorer: ILayoutRestorer, launcher: ILauncher | null) => {
        
        console.log('JupyterLab extension ipydashboard is activated!');
        console.log("VERSION__"+version);
        
        const { commands } = app;
        
       
        
        
            
            
            
            
        };
        
        const tracker = new NotebookTracker({ namespace: 'notebook' });

        // Set up state restorers
          restorer.restore(tracker, {
            command: 'docmanager:open',
            args: widget => ({ path: widget.context.path, factory: FACTORY }),
            name: widget => widget.context.path
          });
        
        // Add a command for creating a new dashboard file.
        commands.addCommand('ipydashboard:create-new', {
            label: EXTENSIONNAME,
            caption: COMMAND_CAPTION,
            execute: () => {
                let cwd = browserFactory.defaultBrowser.model.path;
                return createNewDashboard(cwd);
            }
        });
        
        // Add a launcher item if the launcher is available.
        if (launcher) {
            launcher.add({
                command: 'ipydashboard:create-new',
                rank: 0,
                category: 'Other'
            });
        }
        
        // Now register the document widget
        let rendermime = new RenderMimeRegistry({ initialFactories });
        //let editorFactory = editorServices.factoryService.newInlineEditor;
        //let contentFactory = new NotebookPanel.ContentFactory({ editorFactory });
        
          let mFactory = new f({});
          let editorFactory = editorServices.factoryService.newInlineEditor;
          let contentFactory = new NotebookPanel.ContentFactory({ editorFactory });

          let wFactory = new w({
            name: FACTORY, fileTypes: [FILEEXTENSION], defaultFor: [FILEEXTENSION],
            modelName: 'dashboard',
            preferKernel: true,
            canStartKernel: true,
            rendermime,
            contentFactory,
            mimeTypeService: editorServices.mimeTypeService
          });
          app.docRegistry.addModelFactory(mFactory);
          app.docRegistry.addWidgetFactory(wFactory);
        /*
        let wFactory = new DashboardWidgetFactory({
            name: FACTORY, fileTypes: [FILEEXTENSION], defaultFor: [FILEEXTENSION],
            modelName: 'notebook',
            preferKernel: true,
            canStartKernel: true,
            rendermime,
            contentFactory,
            mimeTypeService: editorServices.mimeTypeService
        });
        app.docRegistry.addWidgetFactory(wFactory);*/

        //see: https://jupyterlab.github.io/jupyterlab/classes/_docregistry_src_registry_.documentregistry.html
        //const wFactory = new DashboardFactory({ name: FACTORY, fileTypes: [FILEEXTENSION], defaultFor: [FILEEXTENSION] });
        
        // register the filetype
        app.docRegistry.addFileType({
            name: FILEEXTENSION,
            displayName: EXTENSIONNAME,
            mimeTypes: ['application/ipyds'],
            extensions: [FILEEXTENSION],
            iconClass: 'jp-MaterialIcon jp-ImageIcon',
            fileFormat: 'json'
        });

        wFactory.widgetCreated.connect((sender, widget) => {
            console.log("connected");
            console.log("sender");
            console.log(widget);
            //widget.title.icon = 'jp-MaterialIcon jp-ImageIcon';
            tracker.save(widget);
            // Notify the instance tracker if restore data needs to update.
            // widget.context.pathChanged.connect(() => { tracker.save(widget); });
            //widget.activate();
            // Notify the instance tracker if restore data needs to update.
            widget.context.pathChanged.connect(() => {
              tracker.save(widget);
            });
            // Add the notebook panel to the tracker.
            tracker.add(widget);
            console.log(widget);
        });
        
    }
};



export default plugin;

/*
displayName: EXTENSIONNAME,
                name: EXTENSIONNAME,
                iconClass: 'jp-MaterialIcon jp-ImageIcon',
                


function activate(app: JupyterLab,
                  
                  restorer: ILayoutRestorer, 
                  menu: IMainMenu,
                  
                  ): IpyDashboardTracker {
    const namespace = 'ipydashboard';
    
    const { commands } = app;
    const tracker = new InstanceTracker<DashboardWidget>({ namespace });

    

    // Handle state restoration.
    
    restorer.restore(tracker, {
        command: 'docmanager:open',
        args: widget => ({ path: widget.context.path, factory: FACTORY }),
        name: widget => widget.context.path
    });

    
    
    app.docRegistry.addWidgetFactory(factory);

    // register the filetype
    app.docRegistry.addFileType({
        name: FILEEXTENSION,
        displayName: EXTENSIONNAME,
        mimeTypes: ['application/jpconfig'],
        extensions: [FILEEXTENSION],
        iconClass: 'jp-MaterialIcon jp-ImageIcon',
        fileFormat: 'json'
    });

    

    // Add a command for creating a new dashboard file.
    commands.addCommand('jupyter_config:create-new', {
        label: EXTENSIONNAME,
        caption: COMMAND_CAPTION,
        execute: () => {
            let cwd = browserFactory.defaultBrowser.model.path;
            return createNewDashboard(cwd);
        }
    });

    // Add a launcher item if the launcher is available.
    if (launcher) {
        launcher.add({
            displayName: EXTENSIONNAME,
            name: EXTENSIONNAME,
            iconClass: 'jp-MaterialIcon jp-ImageIcon',
            callback: createNewDashboard,
            rank: 1,
            category: 'Other'
        });
    }

    if (menu) {
        // Add new file creation to the file menu.
        menu.fileMenu.newMenu.addGroup([{ command: 'ipydashboard:create-new' }], 40);
    }

    return tracker;
}*/